# OpenCV stereo calibration
This calibration routine calibrates a stereo setup based on image pairs.

# How to calibrate
* Place your image pairs in ./images directory. The first images should be called for example imageA1.jpg and imageB1.jpg
* Adjust corner_length in source code.
* Execute ./stereocalibrate numBoards board_w board_h.
